package usthon.syafii.aplikasi08

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.constraintlayout.widget.ConstraintLayout
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {


    lateinit var preferences : SharedPreferences
    val PREF_NAME = "setting"
    var BG_COLOR = "bg_color"
    var HDR_COLOR = "hdr_color"
    var SUBTITLE_FONT_SIZE = "head_font_size"
    var DETAIL_FONT_SIZE = "title_font_size"
    var TITLE_TEXT = "text"
    var DETAIL_TEXT = "text"
    val DEF_BG_COLOR = "WHITE"
    val DEF_HDR_COLOR = "WHITE"
    val DEF_SUBTITLE_FONT_SIZE = 18
    val DEF_DETAIL_FONT_SIZE = 14
    val DEF_TITLE_TEXT = "TOY STORY 4"
    val DEF_DETAIL_TEXT = "Toy Story merupakan Serial Film Animasi Anak Anak dari Disney"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    override fun onStart() {
        super.onStart()
        loadSetting()
    }

    fun loadSetting(){
        preferences = getSharedPreferences(PREF_NAME,Context.MODE_PRIVATE)
        val DetailLayout = findViewById(R.id.detail) as ConstraintLayout
        val HeaderLayout = findViewById(R.id.header) as ConstraintLayout
        DetailLayout.setBackgroundColor(Color.parseColor(preferences.getString(BG_COLOR,DEF_BG_COLOR)))
        HeaderLayout.setBackgroundColor(Color.parseColor(preferences.getString(HDR_COLOR,DEF_HDR_COLOR)))
        txSubTitle.textSize = preferences.getInt(SUBTITLE_FONT_SIZE,DEF_SUBTITLE_FONT_SIZE).toFloat()
        txDetail.textSize = preferences.getInt(DETAIL_FONT_SIZE,DEF_DETAIL_FONT_SIZE).toFloat()
        txTitle.setText(preferences.getString(TITLE_TEXT,DEF_TITLE_TEXT))
        txDetail.setText(preferences.getString(DETAIL_TEXT,DEF_DETAIL_TEXT))
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        var mnuInflater = menuInflater
        mnuInflater.inflate(R.menu.option_menu,menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item?.itemId){
            R.id.itemSetting ->{
                var intent = Intent(this,SettingActivity::class.java)
                startActivity(intent)
            }
        }
        return super.onOptionsItemSelected(item)
    }
}
