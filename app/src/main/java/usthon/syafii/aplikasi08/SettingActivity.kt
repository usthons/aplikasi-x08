package usthon.syafii.aplikasi08

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.SeekBar
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_setting.*

class SettingActivity : AppCompatActivity(), View.OnClickListener, SeekBar.OnSeekBarChangeListener{

    lateinit var preferences : SharedPreferences
    val PREF_NAME = "setting"
    var BG_COLOR = "bg_color"
    var HDR_COLOR = "hdr_color"
    var SUBTITLE_FONT_SIZE = "head_font_size"
    var DETAIL_FONT_SIZE = "title_font_size"
    var DETAIL_TEXT = "text"
    var TITLE_TEXT = "text"
    val DEF_BG_COLOR = "WHITE"
    val DEF_HDR_COLOR = "WHITE"
    val DEF_SUBTITLE_FONT_SIZE = 18
    val DEF_DETAIL_FONT_SIZE = 14
    val DEF_TITLE_TEXT = "TOY STORY"
    val DEF_DETAIL_TEXT = "Toy Story merupakan Serial Film Animasi Anak Anak dari Disney"
    var HeaderColor : String = ""
    var BGColor : String = ""
    val arrayBGColor = arrayOf("BLUE","YELLOW","GREEN","BLACK")
    lateinit var adapterSpin : ArrayAdapter<String>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_setting)
        //listener
        btnSimpan.setOnClickListener(this)
        rgBGHColor.setOnCheckedChangeListener { group, checkedId ->
            when(checkedId){
                R.id.rbBlue -> HeaderColor = "BLUE"
                R.id.rbYellow -> HeaderColor = "YELLOW"
                R.id.rbGreen -> HeaderColor = "GREEN"
                R.id.rbBlack -> HeaderColor = "BLACK"
            }
        }
        adapterSpin = ArrayAdapter(this,android.R.layout.simple_list_item_1,arrayBGColor)
        spBGColor.adapter = adapterSpin
        spBGColor.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                BGColor = adapterSpin.getItem(position).toString()
            }
        }
        //get resources from SharedPreferences
        preferences = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
        var spinnerselection = adapterSpin.getPosition(preferences.getString(BG_COLOR,DEF_BG_COLOR))
        spBGColor.setSelection(spinnerselection,true)
        var rgselection = preferences.getString(HDR_COLOR,DEF_HDR_COLOR)
        if (rgselection=="BLUE"){
            rgBGHColor.check(R.id.rbBlue)
        }else if(rgselection=="YELLOW"){
            rgBGHColor.check(R.id.rbYellow)
        }else if(rgselection=="GREEN"){
            rgBGHColor.check(R.id.rbGreen)
        }else{
            rgBGHColor.check(R.id.rbBlack)
        }
        sbFSubTitle.progress = preferences.getInt(SUBTITLE_FONT_SIZE,DEF_SUBTITLE_FONT_SIZE)
        sbFSubTitle.setOnSeekBarChangeListener(this)
        sbFDetailIsi.progress = preferences.getInt(DETAIL_FONT_SIZE,DEF_DETAIL_FONT_SIZE)
        sbFDetailIsi.setOnSeekBarChangeListener(this)
        edTitle.setText(preferences.getString(TITLE_TEXT,DEF_TITLE_TEXT))
        edDetail.setText(preferences.getString(DETAIL_TEXT,DEF_DETAIL_TEXT))
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnSimpan ->{
                //save configuration to SharedPreferences
                preferences = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
                val prefEditor = preferences.edit()
                prefEditor.putString(BG_COLOR,BGColor)
                prefEditor.putString(HDR_COLOR,HeaderColor)
                prefEditor.putInt(SUBTITLE_FONT_SIZE,sbFSubTitle.progress)
                prefEditor.putInt(DETAIL_FONT_SIZE,sbFDetailIsi.progress)
                prefEditor.putString(TITLE_TEXT,edTitle.text.toString())
                prefEditor.putString(DETAIL_TEXT,edDetail.text.toString())
                prefEditor.commit()
                Toast.makeText(this,"Perubahan Disimpan",Toast.LENGTH_SHORT).show()
            }
        }
    }

    override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
        when(seekBar?.id){
            R.id.sbFSubTitle ->{

            }
            R.id.sbFDetailIsi ->{

            }
        }
    }

    override fun onStartTrackingTouch(seekBar: SeekBar?) {

    }

    override fun onStopTrackingTouch(seekBar: SeekBar?) {

    }
}